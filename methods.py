from PIL import Image, ImageTk
import numpy as np
import scipy.cluster
import cv2
import random
import math
import skimage


def dominant_contour(background):

    # find dominant color
    num_clusters = 5

    print('reading image')
    im = background.copy()
    ar = np.asarray(im)
    shape = ar.shape
    ar = ar.reshape(scipy.product(shape[:2]), shape[2]).astype(float)

    print('finding clusters')
    codes, dist = scipy.cluster.vq.kmeans(ar, num_clusters)
    print('cluster centres:\n', codes)

    vecs, dist = scipy.cluster.vq.vq(ar, codes)  # assign codes
    counts, bins = scipy.histogram(vecs, len(codes))  # count occurrences

    index_max = scipy.argmax(counts)  # find most frequent
    color1 = codes[index_max]

    counts[index_max] = 0
    index_max = scipy.argmax(counts)
    color2 = codes[index_max]
    print(counts)
    print(color2)
    print('dominant color: 0x' + ''.join(hex(int(c))[2:] for c in color1))
    print('2nd dominant color: 0x' + ''.join(hex(int(c))[2:] for c in color2))

    # cv2 load the image
    image = cv2.cvtColor(np.asarray(background.copy().convert("RGB")), cv2.COLOR_RGB2BGR)
    #image = np.asarray(background.copy().convert("RGB"))[:,:,::-1].copy()

    # two color boundaries: color1 - the most dominant, color2 - 2nd best
    boundaries = [([color1[1] - 10, color1[2] - 10, color1[0] - 10], [240, 240, 240])]

    # loop over the boundaries
    for (lower, upper) in boundaries:
        # create NumPy arrays from the boundaries
        lower = np.array(lower, dtype="uint8")
        upper = np.array(upper, dtype="uint8")

        # find the colors within the specified boundaries and apply the mask
        mask = cv2.inRange(image, lower, upper)
        output = cv2.bitwise_and(image, image, mask=mask)

        ret, thresh = cv2.threshold(mask, 40, 255, 0)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        if len(contours) != 0:
            # draw in blue all the contours
            cv2.drawContours(output, contours, -1, 255, 3)

            # find the biggest area
            c = max(contours, key=cv2.contourArea)

            x, y, w, h = cv2.boundingRect(c)
            # draw the best contour (in green)
            cv2.rectangle(output, (x, y), (x + w, y + h), (0, 255, 0), 2)

            M = cv2.moments(c)
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            cv2.circle(output, (cX, cY), 7, (40, 255, 0), -1)
            cv2.putText(output, "center", (cX - 20, cY - 20),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (40, 255, 0), 2)

            # show the images
            # cv2.imshow("images", np.hstack([image, output]))
            # cv2.waitKey(0)

            return [cX, cY]


def random_position(background):
    bg_width, bg_height = background.size
    x = random.randint(0, bg_width)
    y = random.randint(0, bg_height)
    print(int(x), int(y))
    return [x, y]


def tiles(input_background):
    background = input_background.convert('LA')
    im = np.array(background)
    # skimage.measure.shannon_entropy(im)
    # print(calculate_entropy(im))

    M = 240
    N = 240
    tiles = [im[x:x + M, y:y + N] for x in range(0, im.shape[0], M) for y in range(0, im.shape[1], N)]
    positions = [[x, y] for x in range(0, im.shape[0], M) for y in range(0, im.shape[1], N)]
    print(positions)

    best_tile = tiles[0]
    best_score = 100000000000

    inc = 0
    position_index = 0

    for tile in tiles:
        None
        # print(skimage.measure.shannon_entropy(tile))
        score = skimage.measure.shannon_entropy(tile)

        if(score < best_score):
            best_score = score
            best_tile = tile
            position_index = inc
        inc = inc + 1
        # new_im = Image.fromarray(tile)
        # new_im.show()

    new_im = Image.fromarray(best_tile)
    #new_im.show()
    print(len(tiles))
    coord = positions[position_index]
    x = coord[0] + (M/2)
    y = coord[1] + (N/2)

    bg_width, bg_height = background.size

    if(x > bg_width):
        x = bg_width - 100

    if(y > bg_height):
        y = bg_height - 100

    print(int(x), int(y))
    return [int(x), int(y)]

def available_methods():
    return {
        'rand': random_position,
        'dominant': dominant_contour,
        'tiles': tiles
    }