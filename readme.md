testowane na python36

potrzebne moduły:
- Pillow
- numpy
- opencv-python
- scipy
- scikit-image

# co i jak
- w \_\_init\_\_ jest main() gdzie zdefiniowane są nazwy obrazków
- foreground (fg) to wklejany obiekt
- background (bg) to zdjęcie biurka
- fg.png jest troche za duży więc jest zmniejszany przy pomocy funkcji scale_down()
- skrypt potrzebuje koordynat [x,y], które mówią w jakim miejscu na biurku wkleić obiekt 
- w funkcji test() oba obrazki są sklejane i powstały obrazek wyświetlany jest jako klikalny label
- po wyświetleniu obrazka użytkownik ma czas na kliknięcie przedmiotu, który uznaje za sztucznie doklejony (limit czasu aktualnie ustawiony na 3 sec)
- wynik testu zapisywany jest do results.txt (true - trafiono w doklejony obrazek; false - nie trafiono; time_ran_out - użytkownik nie zdążył wybrać)

# TO-DO
- zmodyfikować skrypt żeby łatwo to było testować (np. jakiś switch-case gdzie można wybrać jedną z metod nakładania obrazka albo jakiś losowy wybór metody) - dodana opcja "--method"
- można troche zmienić sposób zapisywania wyników testów do results.txt żeby wynikiem było 0 lub 1 a nie true/false - na koncu dodano juz podsumowanie
- większość z obrazków w bazie nie ma cienia, trzeba go ręcznie dorobić w gimpie/photoshopie albo programistycznie w pythonie - zrobione, randomowo dodawany jest cien
- obrazki obiektów i biurek różnią się wielkościami, trzeba ogarnąć sposób żeby odpowiednio dostosowywać rozmiary obiektu do wybranego zdjęcia biurka (musi to wyglądać realistycznie) - nie bedzie to eleganckie, ale mozna sprobowac pozmieniac rozdzielczosci. Na zasadzie, ze rozdzielczosc zdjecia calego biurka to A x B, a polowy biurka proporcjonalnie 0.5A x 0.5B
- bardzo duże obrazki biurek należałoby zmniejszyć do rozdzielczości ~720p, dzięki czemu będzie je łatwiej wyświetlać i obliczenia będą szybsze - skrypt skaluje to sobie w taki sposob, aby obrzek miescil sie w danym prostokacie
- nie otwierać nowego okna dla każdego kolejnego testu, zmieniać tylko obraz