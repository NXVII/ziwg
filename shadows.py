from PIL import Image
from PIL import ImageFilter
from PIL import ImageEnhance
from PIL import ImageChops
from random import randint

def getShadow(image, iterations):
    shadow = image.copy()
    enhancer = ImageEnhance.Brightness(shadow)
    shadow = enhancer.enhance(0.0)
    for i in range(0, iterations):
        shadow = shadow.filter(ImageFilter.BLUR)
    return shadow

def getShadowPosition(position, size):
    offset_range = (int(size[0]*0.08), int(size[1]*0.08))
    return (position[0] + randint(-offset_range[0], offset_range[0]),
            position[1] + randint(-offset_range[1], offset_range[1]))