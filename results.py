from enum import Enum

class TestResult(Enum):
    Success = 0,
    Failure = 1,
    Timeout = 2

class Results:
    def __init__(self):
        self._stats = {
            TestResult.Success : 0,
            TestResult.Failure : 0,
            TestResult.Timeout : 0
            }

    def add(self, result):
        if result is not None:
            self._stats[result] += 1

    def count(self):
        return self.successes() + self.failures() + self.timeouts()

    def successes(self):
        return self._stats[TestResult.Success]

    def failures(self):
        return self._stats[TestResult.Failure]

    def timeouts(self):
        return self._stats[TestResult.Timeout]

    def success_ratio(self):
        return self.successes() / self.count()

    def failure_ratio(self):
        return self.failures() / self.count()

    def timeout_ratio(self):
        return self.timeouts() / self.count()