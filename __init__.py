import tkinter as tk
import os
import random
import glob
import argparse
from methods import *
from results import *
from reporter import FileReporter
from shadows import getShadow
from shadows import getShadowPosition


def test(background, foreground, position, shadow_on):
    #value to store test's result
    result = None

    if shadow_on:
        shadow = getShadow(foreground, 5)
        shadow_position = getShadowPosition(position, foreground.size)
        background.paste(shadow, shadow_position, shadow)
    background.paste(foreground, (position[0], position[1]), foreground)

    fg_width, fg_height = foreground.size

    # init
    root = tk.Tk()
    photo = ImageTk.PhotoImage(background)

    # label with image
    l = tk.Label(root, image=photo)
    l.pack()

    def get_origin(eventorigin):
        nonlocal result
        global x, y
        x = eventorigin.x
        y = eventorigin.y
        print(x, y)
        if position[0] <= x <= position[0] + fg_width and position[1] <= y <= position[1] + fg_height:
            print('true')
            result = TestResult.Success
        else:
            print('false')
            result = TestResult.Failure

        root.destroy()

    root.bind("<Button 1>", get_origin)

    def countdown(time):
        nonlocal result
        if time == 0:
            result = TestResult.Timeout
            root.destroy()
        else:
            label.configure(text="time remaining: %d seconds" % time)

        root.after(1000, countdown, time - 1)

    label = tk.Label(root, width=30)
    label.pack(padx=20, pady=20)
    countdown(3)

    root.mainloop()
    return result

def run_tests(number_of_tests, find_position_method, shadows_on, reporter, bg_dir, fg_dir):
    results = Results()
    for i in range(0, number_of_tests):
        # input
        bg_name = "bg.jpg"
        fg_name = "fg.png"

        bg_name = random.choice(glob.glob(bg_dir + '/' + "bg*"))
        fg_name = random.choice(glob.glob(fg_dir + '/' + "fg*"))
        print("using bg '" + bg_name + "' and fg '" + fg_name + "'")

        background = Image.open(bg_name).convert("RGBA")
        foreground = Image.open(fg_name).convert("RGBA")

        bg_scale = math.ceil(get_min_scale_factor(background.size, [1280, 720]))
        fg_scale = 7
        background = scale_down(background, bg_scale)
        foreground = scale_down(foreground, fg_scale)

        position = find_position_method(background)

        result = test(background, foreground, position, shadows_on)
        if result is None:
            result_text = "Error occurred"
        else:
            results.add(result)
            result_text = {
                TestResult.Success : "true",
                TestResult.Failure : "false",
                TestResult.Timeout : "time_ran_out"
                }[result]
        reporter.writeln("test {}: {}".format(i, result_text))
    return results

def scale_down(foreground, fg_scale):
    fg_width, fg_height = foreground.size

    fg_width = fg_width // fg_scale
    fg_height = fg_height // fg_scale

    foreground = foreground.resize((fg_width, fg_height))

    return foreground

def get_min_scale_factor(curr_size, dest_size):
    """
    >>> get_min_scale_factor([1400, 600], [700, 600])
    2.0
    >>> get_min_scale_factor([1400, 1500], [700, 600])
    2.5
    >>> get_min_scale_factor([700, 600], [1400, 1500])
    1.0
    """
    min_width_scale_factor = curr_size[0] / dest_size[0]
    min_height_scale_factor = curr_size[1] / dest_size[1]

    factors = [min_width_scale_factor, min_height_scale_factor, 1.0] #at lesat 1
    return max(factors)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--method", type=str, choices = available_methods().keys())
    parser.add_argument("-n", "--number", type=int, default = 1)
    parser.add_argument("-o", "--output", type=str, default = "result.txt")
    parser.add_argument("-b", "--backgrounds", type=str, default = ".")
    parser.add_argument("-f", "--foregrounds", type=str, default = ".")
    parser.add_argument("--shadows", action="store_true")
    args = parser.parse_args()

    find_position_method = available_methods().get(args.method, tiles)
    shadows_on = args.shadows
    number_of_tests = args.number
    reporter = FileReporter(args.output)
    bg_dir = args.backgrounds
    fg_dir = args.foregrounds

    reporter.writeln("Used method: " + find_position_method.__name__)
    
    results = run_tests(number_of_tests, find_position_method, shadows_on, reporter, bg_dir, fg_dir)

    reporter.writeln("all tests: {}".format(results.count()))
    reporter.writeln("successes: {} ({}%)".format(results.successes(), results.success_ratio()*100))
    reporter.writeln("failures: {} ({}%)".format(results.failures(), results.failure_ratio()*100))
    reporter.writeln("timeouts: {} ({}%)".format(results.timeouts(), results.timeout_ratio()*100))

    print("Results available in file " + reporter.filename())

main()
