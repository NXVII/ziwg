import datetime

class FileReporter:
    def __init__(self, file):
        self._file = file
        with open(self._file, "w") as myfile:
            myfile.write("Date: {}\n".format(datetime.datetime.now()))

    def write(self, text):
        with open(self._file, "a") as myfile:
            myfile.write(text)

    def writeln(self, text):
        with open(self._file, "a") as myfile:
            myfile.write(text + '\n')

    def filename(self):
        return self._file